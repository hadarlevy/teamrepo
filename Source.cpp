#include <iostream>
#include <string>
#include "sqlite3.h"
#include <unordered_map>

using namespace std;

typedef unordered_map<string, vector<string>> sql_data;

sql_data *results;
string results_field = "";

void clearTable()
{
	sql_data::iterator it;
	for (it = results->begin(); it != results->end(); ++it)
	{
		it->second.clear();
	}
	results->clear();
	results_field = "";
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		sql_data::iterator it = results->find(azCol[i]);
		if (it != results->end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results->insert(p);
		}
	}

	return 0;
}

int callback_getField(void* notUsed, int argc, char** argv, char** azCol)
{
	if (argc == 1)
	{
		results_field = argv[0];
		return 0;
	}
	else return -1;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	string sql = "select balance from accounts where buyer_ID is " + to_string(buyerid);
	sqlite3_exec(db, "begin transaction", NULL, NULL, &zErrMsg);
	if (sqlite3_exec(db, sql.c_str(), callback_getField, NULL, &zErrMsg) != SQLITE_OK)
	{
		cout << "SQL Error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		int balance = atoi(results_field.c_str());
		sql = "select price from cars where id is " + to_string(carid);
		if (sqlite3_exec(db, sql.c_str(), callback_getField, NULL, &zErrMsg) != SQLITE_OK)
		{
			cout << "SQL Error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
		}
		else
		{
			int price = atoi(results_field.c_str());
			sql = "select available from cars where id is " + to_string(carid);
			if (sqlite3_exec(db, sql.c_str(), callback_getField, NULL, &zErrMsg) != SQLITE_OK)
			{
				cout << "SQL Error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
			}
			else
			{
				if (atoi(results_field.c_str()) == 1 && balance >= price)
				{
					sql = "update cars set available = 0 where id is " + to_string(carid);
					if (sqlite3_exec(db, sql.c_str(), NULL, NULL, &zErrMsg) != SQLITE_OK)
					{
						cout << "SQL Error: " << zErrMsg << endl;
						sqlite3_free(zErrMsg);
					}
					sql = "update accounts set balance = " + to_string(balance - price) + " where id is " + to_string(buyerid);
					if (sqlite3_exec(db, sql.c_str(), NULL, NULL, &zErrMsg) != SQLITE_OK)
					{
						cout << "SQL Error: " << zErrMsg << endl;
						sqlite3_free(zErrMsg);
						sqlite3_exec(db, "rollback", NULL, NULL, &zErrMsg);
					}
					sqlite3_exec(db, "commit", NULL, NULL, &zErrMsg);
					return true;
				}
			}
		}
	}
	return false;
}

void main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0, *sql = "";
	bool flag = true;
	results = new sql_data;

	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Cannot Open Database: " << sqlite3_errmsg(db) << endl;
	}
	else
	{
		clearTable();
		if (carPurchase(5, 9, db, zErrMsg))
			cout << "Success" << endl;
		else cout << "Unsuccessful Purchased" << endl;

		if (carPurchase(3, 23, db, zErrMsg))
			cout << "Success" << endl;
		else cout << "Unsuccessful Purchased" << endl;

		if (carPurchase(12, 22, db, zErrMsg))
			cout << "Success" << endl;
		else cout << "Unsuccessful Purchased" << endl;
	}

	system("PAUSE");
	delete results;
	sqlite3_close(db);
}

/*

#include "sqlite3.h"
#include <iostream>
#include <string>

using namespace std;

string curBalance = "";

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "callback" << endl;
	system("PAUSE");
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	string sql = "select balance from accounts where Buyer_id = '" + std::to_string(buyerid) + "'";
	int rc = sqlite3_exec(db, sql.c_str(), is_balance_ok, 0, &zErrMsg);
	return true;
}

int get_balance(void* notUsed, int argc, char** argv, char** azCol)
{
	curBalance;
}

int is_balance_ok(void* notUsed, int argc, char** argv, char** azCol)
{
	
	return 0;
}

void main()
{
	sqlite3* db;
	char *zErrMsg = 0;
	int rc = sqlite3_open("Cars.db", &db);
	carPurchase(1, 2, db, zErrMsg);
	system("PAUSE");
}

*/
